Gem::Specification.new do |s|
  s.name        = "vbot_client"
  s.version     = '0.0.1'
  s.licenses    = ['MIT']
  s.summary     = "VBOT API client"
  s.description = "VBOT API client"
  s.authors     = ["vbot"]
  s.email       = 'theguys@vbot.tv'
  s.homepage    = 'http://vbot.tv'

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- test/*`.split("\n")
  s.require_paths = ["lib"]
  s.required_ruby_version = '>= 2.0.0'

  s.add_dependency("httparty", "~> 0.13.5")
  s.add_dependency("oauth2", "~> 1.0.0")
  s.add_dependency("hashie", "~> 3.4.3")
end
