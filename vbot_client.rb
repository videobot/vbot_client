require 'rubygems'
require 'oauth2'
require 'json'
require 'hashie'

require './vbot_client_error'

class VbotClient

  def initialize(login, pass, app_id = ENV['VBOT_APP_ID'], app_secret = ENV['VBOT_APP_SECRET'])
    @login      = login
    @pass       = pass
    @app_id     = app_id
    @app_secret = app_secret
    @client     = client

    get_tokens
  end

  def get_videos(opts = {})
    resp = @token.request(:get, '/api/v3/videos')
    parse_response(resp)
  end

  def get_video(id, opts = {})
    resp = @token.request(:get, "/api/v3/videos/#{id}")
    parse_response(resp)
  end

  private

  def client
    ::OAuth2::Client.new(
      @app_id, @app_secret,
      site:           ENV['VBOT_URL'],
      authorize_path: '/oauth/authorize',
      token_path:     '/oauth/token',
      user_type:      'admin'
    )
  end

  def get_tokens
    req = @client.password.get_token(@login, @pass, user_type: 'admin')

    @token = req
    @access_token  = req.token
    @refresh_token = req.refresh_token
  end

  def headers
    {
      'Authorization' => "Bearer #{@access_token}"
    }
  end

  def parse_response(resp)
    json = JSON.parse(resp.body)

    if json.is_a?(Array)
      json.map do |row|
        Hashie::Mash.new(row)
      end
    else
      Hashie::Mash.new(json)
    end
  end
end

# class VbotClient
#   include HTTParty
#
#   parser VbotClientParser
#   base_uri BASE_URI
#   debug_output $stderr
#
#   def initialize(login, pass, app_id = ENV['VBOT_APP_ID'], app_secret = ENV['VBOT_APP_SECRET'])
#     @login      = login
#     @pass       = pass
#     @app_id     = app_id
#     @app_secret = app_secret
#   end
#
#   # Videos
#   #
#   def get_videos(opts = {})
#     auth
#     resp = self.class.get '/videos', query: opts, headers: headers
#     handle_response(resp)
#   end
#
#   def get_video(id, opts = {})
#     auth
#     self.class.get "/videos/#{id}", query: opts
#   end
#
#   def create_video(opts = {})
#     auth
#     self.class.post "/videos", query: opts
#   end
#
#   # Players
#   #
#   def get_players(opts = {})
#     self.class.get '/players', query: opts
#   end
#
#   def get_player(id, opts = {})
#     self.class.get "/players/#{id}", query: opts
#   end
#
#   def create_player(opts = {})
#     self.class.post "/players", query: opts
#   end
#
#   private
#
#   def client
#     ::OAuth2Client::Client.new(BASE_URI, @app_id, @app_secret)
#   end
#
#   def auth
#     @token = client.password.get_token(@login, @pass)
#   end
#
#   def headers
#     {'Auth-Token' => "Bearer #{@token}"}
#   end
#
#   def handle_response(resp)
#     case resp.code.to_i
#     when 200...201
#       resp.parsed_response
#     when 404
#       raise VbotClientError::NotFound
#     when 401
#       raise ::VbotClientError::Unauthorized
#     else
#       raise VbotClientError::Unknown
#     end
#   end
# end
