class VbotClientError; end

require './vbot_client_error/unauthorized'
require './vbot_client_error/not_found'
require './vbot_client_error/unknown'
